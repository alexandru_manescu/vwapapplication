
public class VWAP {
	
	private double bid, offer;
	
	public VWAP(double bid, double offer) {
		
		this.bid = bid;
		this.offer = offer;
	}
	
	public double getBid() {
		return bid;
	}
	
	public double getOffer() {
		return offer;
	}

}
