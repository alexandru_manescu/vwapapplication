
public class MarketAction implements MarketUpdate{

	Market market;
	TwoWayPrice twoWayPrice;
	
	public MarketAction(Market market, TwoWayPrice twoWayPrice) {
		this.market = market;
		this.twoWayPrice = twoWayPrice;
	}
	
	@Override
	public Market getMarket() {
		// TODO Auto-generated method stub
		return market;
	}

	@Override
	public TwoWayPrice getTwoWayPrice() {
		// TODO Auto-generated method stub
		return twoWayPrice;
	}

}
