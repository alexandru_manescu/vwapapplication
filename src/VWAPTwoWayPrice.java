
public class VWAPTwoWayPrice implements TwoWayPrice{

	double bidAmount, bidPrice, offerAmount, offerPrice;
	Instrument instrument;
	State state;
	
	public VWAPTwoWayPrice(Instrument instrument, double bidAmount, double bidPrice, double offerAmount, double offerPrice, State state) {
	
		this.instrument = instrument;
		this.bidAmount = bidAmount;
		this.bidPrice = bidPrice;
		this.offerAmount = offerAmount;
		this.offerPrice = offerPrice;
		this.state = state;
	}
	
	@Override
	public Instrument getInstrument() {
		// TODO Auto-generated method stub
		return instrument;
	}

	@Override
	public State getState() {
		// TODO Auto-generated method stub
		return state;
	}

	@Override
	public double getBidPrice() {
		// TODO Auto-generated method stub
		return bidPrice;
	}

	@Override
	public double getOfferAmount() {
		// TODO Auto-generated method stub
		return offerAmount;
	}

	@Override
	public double getOfferPrice() {
		// TODO Auto-generated method stub
		return offerPrice;
	}

	@Override
	public double getBidAmount() {
		// TODO Auto-generated method stub
		return bidAmount;
	}
	
	public void setOfferPrice(double offerPrice) {
		this.offerPrice = offerPrice;
	}
	
	public void setBidPrice(double bidPrice) {
		this.bidPrice = bidPrice;
	}
	
	public void setBidAmount(double bidAmount) {
		this.bidAmount = bidAmount;
	}
	
	public void setOfferAmount(double offerAmount) {
		this.offerAmount = offerAmount;
	}

 
}
