import java.util.HashMap;


public class VWAPCalculator implements Calculator {

	
	HashMap<Market, HashMap<Instrument, HashMap<String, Double>>> marketPrices;	
	
	public VWAPCalculator() {
		
		marketPrices = new HashMap<Market, HashMap<Instrument, HashMap<String, Double>>>(50);
	
		for(Market market : Market.values()) {
			
			HashMap<Instrument, HashMap<String, Double>> instruments = new HashMap<Instrument, HashMap<String, Double>>(20); 
			for(Instrument instrument : Instrument.values()) {

				HashMap<String, Double> marketValues = new HashMap<String, Double>();			
				marketValues.put("bidTotal", 0.);
				marketValues.put("bidAmount", 0.);
				marketValues.put("offerTotal", 0.);
				marketValues.put("offerAmount", 0.);
				
				instruments.put(instrument, marketValues);
			}
			
			marketPrices.put(market, instruments);	
		}	
	}
	

	public TwoWayPrice applyMarketUpdate(MarketUpdate twoWayMarketPrice) {
		// TODO Auto-generated method stub
		Market market = twoWayMarketPrice.getMarket();
		TwoWayPrice twoWayPrice = twoWayMarketPrice.getTwoWayPrice();
		Instrument instrument = twoWayPrice.getInstrument();
	
		HashMap<String, Double> updatedInstrument = marketPrices.get(market).get(instrument);
		
		double bidTotal = updatedInstrument.get("bidTotal") + twoWayPrice.getBidAmount() * twoWayPrice.getBidPrice();
		updatedInstrument.put("bidTotal", bidTotal);

		double bidAmount = updatedInstrument.get("bidAmount") + twoWayPrice.getBidAmount();
		updatedInstrument.put("bidAmount", bidAmount);
		
		double offerTotal = updatedInstrument.get("offerTotal") + twoWayPrice.getOfferAmount() * twoWayPrice.getOfferPrice();
		updatedInstrument.put("offerTotal", offerTotal);

		double offerAmount = updatedInstrument.get("offerAmount") + twoWayPrice.getOfferAmount();
		updatedInstrument.put("offerAmount", offerAmount);
		
		
		///////????????????????????
		((VWAPTwoWayPrice)twoWayPrice).setBidAmount(bidAmount);
		((VWAPTwoWayPrice)twoWayPrice).setBidPrice(bidTotal/bidAmount);
		
		
		((VWAPTwoWayPrice)twoWayPrice).setOfferAmount(offerAmount);
		((VWAPTwoWayPrice)twoWayPrice).setOfferPrice(offerTotal/offerAmount);
	
		return twoWayPrice;	
	}

	
	public VWAP obtainVWAP(Market market, Instrument instrument) {
		
		HashMap<String, Double> instrumentValues = marketPrices.get(market).get(instrument);
		
		double bid = instrumentValues.get("bidTotal")/instrumentValues.get("bidAmount");
		double offer = instrumentValues.get("offerTotal")/instrumentValues.get("offerAmount");
		
		return new VWAP(bid, offer);		
	}
	

}
