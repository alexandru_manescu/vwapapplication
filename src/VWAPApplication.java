
public class VWAPApplication {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Calculator vwapCalculator = new VWAPCalculator();
		
		
		TwoWayPrice vwapTwoWayPrice = new VWAPTwoWayPrice(Instrument.INSTRUMENT0, 10, 23, 14, 21, State.FIRM);
		MarketUpdate marketAction = new MarketAction(Market.MARKET0, vwapTwoWayPrice);
		
		TwoWayPrice twp = vwapCalculator.applyMarketUpdate(marketAction);
		System.out.println("BID:" + twp.getBidPrice());
		System.out.println("OFFER:" + twp.getOfferPrice());
		/////OR using the method vwapCalculator.obtainVWAP
		
		
	}

}
